import { useReducer, Dispatch } from 'react'

type TState = string | boolean

interface BoolAction {
  type: 'boolean'
  payload: boolean
}

interface ArrayAction {
  type: 'array'
  payload: string
}

interface SetValueAction {
  type: 'setValue'
  payload: string
}

type Action = BoolAction | ArrayAction | SetValueAction

function reducer(state: TState, action: Action) {
  switch (action.type) {
    case 'boolean':
      return action.payload
    case 'array':
      return action.payload
    case 'setValue':
      return action.payload
    default:
      return state
  }
}

export function useToggleExtended(
  initialArray?: string[],
): [TState, (val?: string) => Dispatch<Action>] {
  const [value, dispatch] = useReducer(reducer, initialArray ? initialArray[0] : false)

  console.log('value now :', value)

  function toggle(val?: string): Dispatch<Action> {
    if (!val && typeof value === 'boolean') {
      dispatch({ type: 'boolean', payload: !value })
      return
    } else if (!val) {
      const currentElemIndex = initialArray.findIndex((el) => el === value)
      const nextElemIndex = currentElemIndex >= initialArray.length - 1 ? 0 : currentElemIndex + 1
      dispatch({ type: 'array', payload: initialArray[nextElemIndex] })
      return
    } else if (initialArray.includes(val)) {
      dispatch({ type: 'setValue', payload: val })
    }
  }

  return [value, toggle]
}
