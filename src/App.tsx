import { useToggleExtended } from './hooks/useToggleExtended'

export function App() {
  const [value, toggle] = useToggleExtended(['a', 'b', 'c', 'd', 'e', 'f', 'g'])
  // const [value, toggle] = useToggleExtended()

  return (
    <div className="m-2 p-3 border rounded">
      <h1 className="text-3xl">Value now: {value.toString()}</h1>
      <div className="flex max-w-48 justify-center gap-2">
        <button onClick={() => toggle()} className="bg-green-200 border rounded p-2">
          Toggle
        </button>
        <br />
        {typeof value !== 'boolean' && (
          <button onClick={() => toggle('c')} className="bg-orange-200 border rounded p-2">
            Toggle to C
          </button>
        )}
      </div>
    </div>
  )
}
